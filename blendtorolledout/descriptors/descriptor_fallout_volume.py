import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorFalloutVolume(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[FALLOUT_VOLUME]"

    @staticmethod
    def get_node_type():
        return "fallout_volume_node"

    @staticmethod
    def render(obj):
        obj_drawing.draw_fallout_volume(obj)
