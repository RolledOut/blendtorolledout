import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorSafeVolume(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[SAFE_VOLUME]"

    @staticmethod
    def get_node_type():
        return "safe_volume_node"

    @staticmethod
    def render(obj):
        obj_drawing.draw_safe_volume(obj)
