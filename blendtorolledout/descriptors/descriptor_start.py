import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorStart(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[START]"

    @staticmethod
    def get_node_type():
        raise RuntimeError("Start nodes do not belong in the scene graph and thus have no node type")

    @staticmethod
    def render(obj):
        obj_drawing.draw_start(obj)
