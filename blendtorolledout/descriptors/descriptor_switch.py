import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorSwitch(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[SW_"

    @staticmethod
    def get_node_type():
        return "switch_node"

    @staticmethod
    def get_node_properties(obj):
        return obj.switch_node_properties

    @staticmethod
    def render(obj):
        obj_drawing.draw_switch(obj)
