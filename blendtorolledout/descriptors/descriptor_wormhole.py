import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorWormhole(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[WH]"

    @staticmethod
    def get_node_type():
        return "wormhole_node"

    @staticmethod
    def get_node_properties(obj):
        return obj.wormhole_node_properties
    
    @staticmethod
    def render(obj):
        obj_drawing.draw_wh(obj)
