from .descriptor_goal import DescriptorGoalB, DescriptorGoalG, DescriptorGoalR
from .descriptor_bumper import DescriptorBumper
from .descriptor_collectible import DescriptorCollectible1, DescriptorCollectible5, DescriptorCollectible10
from .descriptor_wormhole import DescriptorWormhole
from .descriptor_start import DescriptorStart
from .descriptor_fallout_volume import DescriptorFalloutVolume
from .descriptor_safe_volume import DescriptorSafeVolume
from .descriptor_mine import DescriptorMine
from .descriptor_switch import DescriptorSwitch

descriptors = [
    DescriptorStart,
    DescriptorFalloutVolume,
    DescriptorSafeVolume,
    DescriptorMine,
    DescriptorBumper,
    DescriptorCollectible1,
    DescriptorCollectible5,
    DescriptorCollectible10,
    DescriptorWormhole,
    DescriptorSwitch,
    DescriptorGoalB,
    DescriptorGoalG,
    DescriptorGoalR,
]
