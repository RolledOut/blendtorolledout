# REALLY IMPORTANT: If you want to add a new material, add it to the bottom of their respective lists
# Blender stores dropdown menu options on save based on the last selected index 
# If you add something to the middle of the list, you will change the index of the material that last had that index 
# ...as well as everything after it in the list - which will screw up everyone's stages if they use different materials!

# Properties for each material type, in ro_material
# The keys for this dictionary are the type of material in Rolled Out
# For examples of this, see here: https://docs.rolledoutgame.com/custom_content/material/index.html
# The value of each key is an array indicating which attributes each material should have
# Generally attributes that we implement, like texture coord/stage tilt stuff, should go here
mat_props_ro_material = {
    'rolledout:mat_surface_opaque_lit_generic': ['base_color_multiplier', 'emissive_multiplier', 'metallic_multiplier', 'specular_multiplier', 'roughness_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_translucent_lit_generic': ['base_color_multiplier', 'emissive_multiplier', 'opacity_multiplier', 'refraction_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_masked_lit_generic': ['base_color_multiplier', 'emissive_multiplier', 'metallic_multiplier', 'specular_multiplier', 'roughness_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_opaque_unlit_generic': ['emissive_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_translucent_unlit_generic': ['emissive_multiplier', 'opacity_multiplier', 'refraction_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_additive_generic': ['emissive_multiplier', 'opacity_multiplier', 'refraction_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_multiply_generic': ['emissive_multiplier', 'opacity_multiplier', 'refraction_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_overlay_generic': ['emissive_multiplier', 'opacity_multiplier', 'refraction_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_masked_unlit_generic': ['emissive_multiplier', 'tex_coord_multiplier', 'tex_coord_offset', 'stage_tilt'],
    'rolledout:mat_surface_default' : ['stage_tilt'],
    'rolledout:mat_surface_gravity_surface': ['stage_tilt'],
    'rolledout:mat_surface_goal_surface': ['emissive_multiplier', 'stage_tilt'],
    'rolledout:mat_surface_ice_surface': ['stage_tilt'],
    'rolledout:mat_surface_sticky_surface': ['stage_tilt'],
    'rolledout:mat_surface_invisible': [],
}

# Exclusive principled BSDF properties of each material type
# The keys for this dictionary are the type of material in Rolled Out
# The value of each key is an array indicating which Texture2D parameters a Rolled Out material should have
# For examples of this, see here: https://docs.rolledoutgame.com/custom_content/material/base_materials.html
mat_props_ro_wrapper_exclusive = {
    'rolledout:mat_surface_opaque_lit_generic': ['base_color_texture', 'metallic_texture', 'specular_texture', 'roughness_texture', 'emissive_texture', 'normal_texture'],
    'rolledout:mat_surface_translucent_lit_generic': ['base_color_texture', 'emissive_texture', 'opacity_texture', 'refraction_texture'],
    'rolledout:mat_surface_masked_lit_generic': ['base_color_texture', 'metallic_texture', 'specular_texture', 'roughness_texture', 'emissive_texture', 'opacity_mask_texture', 'normal_texture'],
    'rolledout:mat_surface_opaque_unlit_generic': ['emissive_texture'],
    'rolledout:mat_surface_translucent_unlit_generic': ['emissive_texture', 'opacity_texture', 'refraction_texture'],
    'rolledout:mat_surface_additive_generic': ['emissive_texture', 'opacity_texture', 'refraction_texture'],
    'rolledout:mat_surface_multiply_generic': ['emissive_texture', 'opacity_texture', 'refraction_texture'],
    'rolledout:mat_surface_overlay_generic': ['emissive_texture', 'opacity_texture', 'refraction_texture'],
    'rolledout:mat_surface_masked_unlit_generic': ['emissive_texture', 'opacity_mask_texture'],
    'rolledout:mat_surface_default' : [],
    'rolledout:mat_surface_gravity_surface': [],
    'rolledout:mat_surface_goal_surface': [],
    'rolledout:mat_surface_ice_surface': [],
    'rolledout:mat_surface_sticky_surface': [],
    'rolledout:mat_surface_invisible': [],
}

# PrincipledBSDFWrapper equivalent attributes for material types
# The keys for this dictionary are the names of each parameter as they appear in the Rolled Out config file format 
# The value of each key is a string indicating the corresponding attribute in the PrincipledBSDFWrapper shader
mat_props_wrapper_attribs = {
    'base_color_texture': 'base_color_texture',
    'metallic_texture': 'metallic_texture',
    'specular_texture': 'specular_texture',
    'roughness_texture': 'roughness_texture',
    'emissive_texture': 'emission_color_texture',
    'normal_texture': 'normalmap_texture',
    'opacity_texture': 'alpha_texture',
    'opacity_mask_texture': 'alpha_texture',
    'refraction_texture': 'ior_texture',

    # 'base_color_multiplier': 'base_color',
    # 'emissive_multiplier': 'emission_color',

    'metallic_multiplier': 'metallic',
    'specular_multiplier': 'specular',
    'roughness_multiplier': 'roughness',
    'opacity_multiplier': 'alpha',
    'refraction_multiplier': 'ior',
}

# List of scalar property names
mat_scalar_prop_name_list = [
    "metallic_multiplier",
    "specular_multiplier",
    "roughness_multiplier",
    "refraction_multiplier",
    "opacity_multiplier",
    "stage_tilt"
]

# List of vector2 property names
mat_vector2_prop_name_list = [
    "tex_coord_multiplier",
    "tex_coord_offset"
]

# List of vector3 property names
mat_vector3_prop_name_list = [
    "base_color_multiplier",
    "emissive_multiplier"
]