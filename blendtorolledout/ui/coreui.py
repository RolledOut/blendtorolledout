import bgl
import blf


def draw_box_2d(x, y, w, h, color=(0.0, 0.0, 0.0, 1.0)):
    bgl.glColor4f(*color)
    bgl.glBegin(bgl.GL_QUADS)

    bgl.glVertex2f(x + w, y + h)
    bgl.glVertex2f(x, y + h)
    bgl.glVertex2f(x, y)
    bgl.glVertex2f(x + w, y)

    bgl.glEnd()


def draw_text_2d(color, text, pos, size=12):
    font_id = 0  # XXX, need to find out how best to get this.
    # Draw some text
    bgl.glColor4f(*color)
    blf.position(font_id, pos[0], pos[1], 0.0)
    blf.size(font_id, size, 72)
    blf.draw(font_id, text)

    # blf drawing disables GL_BLEND
    bgl.glEnable(bgl.GL_BLEND)


def pos_to_viewport_space(pos, viewport_size):
    return [pos[0], viewport_size[1] - pos[1]]


def size_to_viewport_space(size):
    return [size[0], -size[1]]


def is_point_over_area(point_pos, area_pos, area_size) -> bool:
    return area_pos[0] < point_pos[0] < area_pos[0] + area_size[0] and \
           area_pos[1] < point_pos[1] < area_pos[1] + area_size[1]
